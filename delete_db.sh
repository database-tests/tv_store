#!/bin/bash

USER=$(whoami)
DATABASE="tv_store"
if psql -lqt | cut -d \| -f 1 | grep -qw $DATABASE; then
    echo "Removendo o banco de dados $DATABASE..."
    dropdb -U "${USER}" --force $DATABASE
    echo "Banco de dados removido com sucesso!"
else
    echo "O banco de dados $DATABASE não existe."
fi

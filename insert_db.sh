#!/bin/bash

USER=$(whoami)
DATABASE="tv_store"
en_uf_data=(
"('U01', 'Rio de Janeiro', 'RJ')"
"('U02', 'São Paulo', 'SP')"
"('U03', 'Minas Gerais', 'MG')"
)

en_municipio_data=(
"('M01', 'Rio de Janeiro')"
"('M02', 'São Paulo')"
"('M03', 'Belo Horizonte')"
)

en_endereco_loja_data=(
"('E01', '50', '88075310', 'Casa', 'Rua A', 'M01', 'U01')"
"('E02', '37', '88542211', 'Ap202', 'Rua B', 'M02', 'U02')"
"('E03', '42', '88756300', 'Casa', 'Rua C', 'M03', 'U03')"
)

en_produto_data=(
"('PR01', '42', 'Samsung', 'LED', 'Modelo A')"
"('PR02', '55', 'LG', 'OLED', 'Modelo B')"
"('PR03', '65', 'Sony', 'QLED', 'Modelo C')"
)

en_loja_cliente_data=(
"('L01', 'contato1@loja.com', '11111111', 'Loja A', 'E01')"
"('L02', 'contato2@loja.com', '22222222', 'Loja B', 'E02')"
"('L03', 'contato3@loja.com', '33333333', 'Loja C', 'E03')"
)

en_vendedor_data=(
"('V01', 'Romualdo')"
"('V02', 'Adriana')"
"('V03', 'Jefferson')"
"('V04', 'Bruno')"
)

en_pedido_data=(
"('PE01', '2023-03-19 10:00:00', 'L01', 'V01')"
"('PE02', '2023-04-19 11:00:00', 'L02', 'V02')"
"('PE03', '2023-05-19 13:00:00', 'L03', 'V03')"
)

re_produto_pedido_data=(
"('5', '1500.00', '10.0', 'PE01', 'PR01')"
"('10', '2000.00', '15.0', 'PE02', 'PR02')"
"('15', '5000.00', '20.0', 'PE03', 'PR03')"
)

echo ""
echo "################ INSERT VALUES ################"
echo ""

for uf_data in "${en_uf_data[@]}"
do
psql -d $DATABASE -c "INSERT INTO EN_UF (COD_UF, NOME_UF, SIGLA_UF) VALUES ${uf_data};"
done

for municipio_data in "${en_municipio_data[@]}"
do
psql -d $DATABASE -c "INSERT INTO EN_MUNICIPIO (COD_MUNICIPIO, NOME_MUNICIPIO) VALUES ${municipio_data};"
done

for endereco_loja_data in "${en_endereco_loja_data[@]}"
do
psql -d $DATABASE -c "INSERT INTO EN_ENDERECO_LOJA (COD_ENDERECO_LOJA, NUMERO_RUA, CEP, COMPLEMENTO, NOME_RUA, COD_MUNICIPIO, COD_UF) VALUES ${endereco_loja_data};"
done

for vendedor_data in "${en_vendedor_data[@]}"
do
psql -d $DATABASE -c "INSERT INTO EN_VENDEDOR (COD_VENDEDOR, NOME_VENDEDOR) VALUES ${vendedor_data};"
done

for produto_data in "${en_produto_data[@]}"
do
psql -d $DATABASE -c "INSERT INTO EN_PRODUTO (COD_PRODUTO, TAMANHO_TELA, MARCA_TV, TIPO_TELA, MODELO_TV) VALUES ${produto_data};"
done

for loja_cliente_data in "${en_loja_cliente_data[@]}"
do
psql -d $DATABASE -c "INSERT INTO EN_LOJA_CLIENTE (COD_LOJA_CLIENTE, EMAIL_CONTATO, TELEFONE_CONTATO, NOME_LOJA_CLIENTE, COD_ENDERECO_LOJA) VALUES ${loja_cliente_data};"
done

for pedido_data in "${en_pedido_data[@]}"
do
psql -d $DATABASE -c "INSERT INTO EN_PEDIDO (COD_PEDIDO, DATA_PEDIDO, COD_LOJA_CLIENTE, COD_VENDEDOR) VALUES ${pedido_data};"
done

for produto_pedido_data in "${re_produto_pedido_data[@]}"
do
psql -d $DATABASE -c "INSERT INTO RE_PRODUTO_PEDIDO (QUANT_ITEM_PEDIDO, VALOR_UNITARIO_PRODUTO, PORCENTAGEM_DESCONTO, COD_PEDIDO, COD_PRODUTO) VALUES ${produto_pedido_data};"
done

echo ""
echo "################ EN_UF ################"
echo ""
psql -d $DATABASE -c "SELECT * FROM EN_UF;"
echo ""
echo "################ EN_MUNICIPIO ################"
echo ""
psql -d $DATABASE -c "SELECT * FROM EN_MUNICIPIO;"
echo ""
echo "################ EN_ENDERECO_LOJA ################"
echo ""
psql -d $DATABASE -c "SELECT * FROM EN_ENDERECO_LOJA;"
echo ""
echo "################ EN_VENDEDOR ################"
echo ""
psql -d $DATABASE -c "SELECT * FROM EN_VENDEDOR;"
echo ""
echo "################ EN_PRODUTO ################"
echo ""
psql -d $DATABASE -c "SELECT * FROM EN_PRODUTO;"
echo ""
echo "################ EN_LOJA_CLIENTE ################"
echo ""
psql -d $DATABASE -c "SELECT * FROM EN_LOJA_CLIENTE;"
echo ""
echo "################ EN_PEDIDO ################"
echo ""
psql -d $DATABASE -c "SELECT * FROM EN_PEDIDO;"
echo ""
echo "################ RE_PRODUTO_PEDIDO ################"
echo ""
psql -d $DATABASE -c "SELECT * FROM RE_PRODUTO_PEDIDO;"
#!/bin/bash
DATABASE="tv_store"
echo ""
echo "################ EN_UF ################"
echo ""
psql -d $DATABASE -c "SELECT * FROM EN_UF;"
echo ""
echo "################ EN_MUNICIPIO ################"
echo ""
psql -d $DATABASE -c "SELECT * FROM EN_MUNICIPIO;"
echo ""
echo "################ EN_ENDERECO_LOJA ################"
echo ""
psql -d $DATABASE -c "SELECT * FROM EN_ENDERECO_LOJA;"
echo ""
echo "################ EN_VENDEDOR ################"
echo ""
psql -d $DATABASE -c "SELECT * FROM EN_VENDEDOR;"
echo ""
echo "################ EN_PRODUTO ################"
echo ""
psql -d $DATABASE -c "SELECT * FROM EN_PRODUTO;"
echo ""
echo "################ EN_LOJA_CLIENTE ################"
echo ""
psql -d $DATABASE -c "SELECT * FROM EN_LOJA_CLIENTE;"
echo ""
echo "################ EN_PEDIDO ################"
echo ""
psql -d $DATABASE -c "SELECT * FROM EN_PEDIDO;"
echo ""
echo "################ RE_PRODUTO_PEDIDO ################"
echo ""
psql -d $DATABASE -c "SELECT * FROM RE_PRODUTO_PEDIDO;"
echo "
 _________________________
|                         |
|   U  = EN_UF            |
|   M  = EN_MUNICIPIO     |
|   E  = EN_ENDERECO_LOJA |
|   V  = EN_VENDEDOR      |
|   PR = EN_PRODUTO       |
|   L  = EN_LOJA_CLIENTE  |
|   PE = EN_PEDIDO        |
|_________________________|
"
# TV_STORE

Essas scripts foram escritas com intuito de automatizar uma atividade de faculdade.

## Instalar o PostgreSQL

 * Para instalar o PostgreSQL siga o passo a passo do site: https://www.postgresql.org/download/
 * Procedimento de instalação no Ubuntu:
```
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
```
```
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
```
```
sudo apt-get update
```
```
sudo apt-get -y install postgresql
```

## Configurar usuário
 * Configure seu usuario para trabalhar no PostgreSQL:
```
sudo -u postgres -i
```
```
createuser -l -d -P <SEU_USUARIO>
```

## Modo de usar
 * Cria o banco de dados e sua estrutura
```
./create_db.sh
```
  * Insere os dados nas tabelas
```
./insert_db.sh
```
 * Exibe todas as tabelas
```
./show_db.sh
```
  * Limpa os dados de todas as tabelas
```
./remove_values_db.sh
```
 * Deleta o banco de dados
```
./delete_db.sh
```
## Questão
Uma importadora de TVs vende seus produtos, para diversas lojas
distribuídas pelo país, deseja construir um sistema transacional para
automatizar o processo dos pedidos dos produtos e ter o controle dessa
operação. Os requisitos levantados são:

- A empresa tem uma lista de produtos (TVs) que importa com as principais
caraterísticas técnicas do produto como marca da TV, modelo, tamanho, tipo
de tela;- a empresa tem também uma lista das lojas as quais vende o produto,
com informações sobre nome da loja, endereço com município e estado,
telefone e e-mail de contato;

- Existem uma lista de vendedores da loja importadora que levanta os pedidos
junto as lojas clientes;

- Os pedidos têm os produtos solicitados, o valor unitário do produto e a
porcentagem de desconto desse ´produto.

Construir um banco de dados relacional do modelo entidaderelacionamento que atenda aos requisitos citados.

## Diagrama
<img src="img/diagrama.png">
